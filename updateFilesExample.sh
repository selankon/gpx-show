#!/bin/bash

FILELIST='filelist.json'
GPXPATH='gpxtracks/'
SERVERPATH='/path/to/git/repo'
SERVER='username@server.dom'

# Create filelist.json
tree -J gpxtracks/ > $FILELIST

# Copy files to the server (only non existent)
rsync -avh -e ssh $GPXPATH  $SERVER:$SERVERPATH$GPXPATH
# Copy the update of the filelist
scp $FILELIST $SERVER:$SERVERPATH
