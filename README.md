# Gpx show

Show gpx tracks on a leaflet map.

It takes from a JSON the name of the files and show the list on a slide menu.
After you can select what track to see.

The JSON is generated using `tree` command, you can attach it to a crontab to get
the GPX file list updated. An example:

```bash
tree -J gpxtracks/ > filelist.json
```

Take a look on `updateFiles.sh`

### Dependencies

* Leaflet 1.3.1
* [Leaflet GPX](https://github.com/mpetazzoni/leaflet-gpx)
* [Leaflet slide menu](https://github.com/unbam/Leaflet.SlideMenu)
* `sudo apt install tree`
